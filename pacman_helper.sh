#!/bin/bash
#Export
file02=/usr/local/bin/pkglist.sh
file03=/usr/bin/lostfiles
file04=/usr/bin/fetchmirrors
file05=/usr/bin/pacman-mirrors
file06=/usr/bin/reflector
file07=/usr/bin/paclog-pkglist
file08=/usr/bin/git
file09=/usr/bin/asp
#file10=/usr/bin/ufw
#file11=/usr/bin/iptables
file12=/usr/bin/nginx
#CheckOS
check01=/etc/os-release
check02=/etc/arch-release
#Функция 
aspsync(){
asp list-all &> /dev/null && sleep 5s
}
#Для 13 пункта
IP=$(ifconfig | sed -En 's/127.0.0.1//;s/.*inet (addr:)?(([0-9]*\.){3}[0-9]*).*/\2/p')
##Проверка условий для запуска скрипта
##Проверка привелегий
if [ "$(id -u)" -eq 0 ]; then
   echo -e "Запустите скрипт от обычного пользователя! \e[1;31mFAIL!\033[0m"
   exit 1
  else
echo -e "Проверка наличия прав...      \e[1;32mOK!\033[0m"
fi
##Проверка требуемой ОС
echo -e "\e[1;32m>>\033[0mПроверка требуемой ОС"
if [[ -f "$check01" ]]; then
echo -e "Найден файл os-release...     \e[1;32mOK!\033[0m"
	else
echo -e "Не найден os-release!... \e[1;31mFAIL!\033[0m"
exit 1
fi
if [[ -f "$check02" ]]; then
echo -e "Найден файл arch-release...   \e[1;32mOK!\033[0m"
	else
echo -e "Не найден arch-release...       \e[1;33mFAIL!\033[0m"
fi
source "$check01"
if [[ "$ID" == "arch" ]]; then
echo -e "У вас \e[1;36mArch Linux\033[0m!"
OS="arch_linux"
elif [[ "$ID" == "manjaro" ]]; then
echo -e "У вас \e[1;32mManjaro\033[0m!"
OS="manjaro_linux"
else
exit 1
fi
echo -e "\e[1;32m>>\033[0mПроверка наличия скриптов и двоичных файлов"
##Проверка скриптов
#Проверка для пункта 1
if [ -f $file07 ]; then
echo -e "Проверка наличия пакета для пункта 1...      \e[1;32mOK!\033[0m"
	ready01="\e[1;32m✔\033[0m"
else
echo -e "Проверка наличия пакета для пункта 1... \e[1;31mFAIL!\033[0m"
echo "Установите пакет pacman-contrib"
echo -e "\e[1;37mУстановить?\033[0m \e[1;32mY\033[0m/\\e[1;31mN\033[0m"
until [[ "$install_script01" == Y || "$install_script01" == y  ||  "$install_script01" == N || "$install_script01" == n ]]
	do
		read -p " " install_script01
	done
	if [[ $install_script01 == Y ]] || [[ $install_script01 == y ]]; then
	sudo pacman -Sy pacman-contrib --noconfirm
 elif [[ $install_script01 == N ]] || [[ $install_script01 == n ]]; then
	fail01="\e[1;31m⨯\033[0m"
	echo "Окей,установки не будет"
fi
 fi
#Проверка для пункта 2
if [ -f $file02 ]; then
echo -e "Проверка наличия скрипта для пункта 2...     \e[1;32mOK!\033[0m"
	ready02="\e[1;32m✔\033[0m"
else
echo -e "Проверка наличия скрипта для пункта 2... \e[1;31mFAIL!\033[0m"
echo "Установите скрипт"
echo -e "\e[1;37mУстановить?\033[0m \e[1;32mY\033[0m/\\e[1;31mN\033[0m"
echo "Для установки требуются рут права!"
	until [[ "$install_script02" == Y || "$install_script02" == y ||  "$install_script02" == N || "$install_script02" == n  ]]
	do
		read -p " " install_script02
	done
	if [[ $install_script02 == Y ]] || [[ $install_script02 == y ]]; then
		sudo wget https://gitlab.com/naruto522ru/pkglist/raw/master/pkglist.sh -P /usr/local/bin/
 	sudo chmod +x /usr/local/bin/pkglist.sh
 elif [[ $install_script02 == N ]] || [[ $install_script02 == n ]]; then
	fail02="\e[1;31m⨯\033[0m"
	echo "Окей,установки не будет"
fi
fi
#Проверка для пункта 3
if [ -f $file03 ]; then
ready03="\e[1;32m✔\033[0m"
echo -e "Проверка наличия пакета для пункта 3...      \e[1;32mOK!\033[0m"
else
echo -e "Проверка наличия пакета для пункта 3... \e[1;31mFAIL!\033[0m"
echo -e "Установите пакет lostfiles"
echo -e "\e[1;37mУстановить?\033[0m \e[1;32mY\033[0m/\\e[1;31mN\033[0m"	
	until [[ "$install_script03" == Y  || "$install_script03" == y || "$install_script03" == N || "$install_script03" == n ]]
	do
		read -p " " install_script03
	done
	if [[ $install_script03 == Y ]] || [[ $install_script03 == y ]]; then
	sudo pacman -Sy lostfiles --noconfirm
 elif [[ $install_script03 == N ]] || [[ $install_script03 == n ]]; then
	fail03="\e[1;31m⨯\033[0m"
	echo "Окей,установки не будет"
fi
fi
#Проверка для пункта 5
#Arch
if [[ "$OS" = "arch_linux" ]]; then
if [[ -f "$file04" && -f "$file06" && "$file08" ]]; then
echo -e "Проверка наличия пакета для пункта 5...      \e[1;32mOK!\033[0m"
ready_arch="\e[1;32m✔\033[0m"
else
echo -e "Проверка наличия пакета для пункта 5... \e[1;31mFAIL!\033[0m"
echo -e "Установите пакет reflector,fetchmirrors"
echo -e "\e[1;37mУстановить?\033[0m \e[1;32mY\033[0m/\\e[1;31mN\033[0m"
	until [[ "$install_script05_arch" == Y || "$install_script05_arch" == y ||  "$install_script05_arch" == N || "$install_script05_arch" == n ]]
		do
			read -p " " install_script05_arch
		done
	if [[ $install_script05_arch == Y ]] || [[ $install_script05_arch == y ]]; then
	#Makedeps
	sudo pacman -Sy git --noconfirm --needed
	sudo pacman -Sy reflector --noconfirm
	echo -e "\e[1;33mВнимание!!!\033[0m Должна группа пакетов:base-devel"
cd ~/
	git clone https://aur.archlinux.org/fetchmirrors.git
	cd ~/fetchmirrors/
	makepkg -si
	cd ~/
	sudo rm -r ~/fetchmirrors/
elif [[ $install_script05_arch == N ]] || [[ $install_script05_arch == n ]]; then
fail_arch="\e[1;31m⨯\033[0m"
echo "Окей,установки не будет"
    fi
fi
#Manjaro
elif [[ "$OS" == "manjaro_linux" && -f $file05 ]]; then
ready_manjaro="\e[1;32m✔\033[0m"
echo -e "Проверка наличия пакета для пункта 5...      \e[1;32mOK!\033[0m"
else
echo -e "Проверка наличия пакета для пункта 5... \e[1;31mFAIL!\033[0m"
echo -e "\e[1;37mУстановить?\033[0m \e[1;32mY\033[0m/\\e[1;31mN\033[0m"
until [[ "$install_script05_manjaro" == Y || "$install_script05_manjaro" == y || "$install_script05_manjaro" == N  || "$install_script05_manjaro" == n ]]
			do
				read -p " " install_script05_manjaro
			done
if [[ $install_script05_manjaro == Y ]] || [[ $install_script05_manjaro == y ]]; then
sudo pacman -Sy pacman-mirrors --noconfirm --needed
elif [[ $install_script05_manjaro == N ]] || [[ $install_script05_manjaro == n ]]; then
fail_manjaro="\e[1;31m⨯\033[0m"
echo "Окей,установки не будет"
fi
fi
#Проверка для пункта 9,15
if [[ "$OS" = "arch_linux" ]]; then
if [ -f $file09 ] && [ -f $file08 ] && [[ "$OS" = "arch_linux" ]]; then
	ready10="\e[1;32m✔\033[0m"
echo -e "Проверка наличия пакетов для пункта 9,15...  \e[1;32mOK!\033[0m"
aspsync
else
echo -e "Проверка наличия пакетов для пункта 9,15... \e[1;31mFAIL!\033[0m"
echo -e "Установите пакеты asp,git"
echo -e "\e[1;37mУстановить?\033[0m \e[1;32mY\033[0m/\\e[1;31mN\033[0m"
until [[ "$install_script10" == Y || "$install_script10" == y ||  "$install_script10" == N || "$install_script10" == n ]]
		do
			read -p " " install_script10
		done
	if [[ $install_script10 == Y ]] || [[ $install_script10 == y ]]; then
	sudo pacman -Sy asp git --noconfirm --needed
 	aspsync
 elif [[ $install_script10 == N ]] || [[ $install_script10 == n ]]; then
fail10="\e[1;31m⨯\033[0m"
	echo "Окей,установки не будет"
fi
fi
fi
#Проверка для пункта 13
if [ -f $file12 ] ; then
	ready11="\e[1;32m✔\033[0m"
echo -e "Проверка наличия пакета для пункта 13...     \e[1;32mOK!\033[0m"
else
echo -e "Проверка наличия пакета для пункта 13... \e[1;31mFAIL!\033[0m"
echo -e "Установите пакет nginx"
echo -e "\e[1;37mУстановить?\033[0m \e[1;32mY\033[0m/\\e[1;31mN\033[0m"
until [[ "$install_script11" == Y || "$install_script11" == y || "$install_script11" == N || "$install_script11" == n ]]
	do
		read -p " " install_script11
	done
	if [[ $install_script11 == Y ]] || [[ $install_script11 == y ]]; then
	sudo pacman -Sy nginx --noconfirm
 elif [[ $install_script11 == N ]] || [[ $install_script11 == n ]]; then
fail11="\e[1;31m⨯\033[0m"
	echo "Окей,установки не будет"
fi
fi
clear
echo -e "\e[1;33mPACMAN HELPER\033[0m v01"
echo""
echo "Меню:"
echo -e "\e[1;35m1)\033[0m\e[1;37mПолучение списка пакетов на основе лога пакмана (полезно при повреждении DB пакмана)\033[0m  $ready01 $fail01"
echo -e "\e[1;35m2)\033[0m\e[1;37mПолучение списка пакетов\033[0m $ready02 $fail02"
echo -e "\e[1;35m3)\033[0m\e[1;37mПоиск файлов,которые не принадлежат пакетам\033[0m \e[1;31mROOT\033[0m $ready03 $fail03"
echo -e "\e[1;35m4)\033[0m\e[1;37mОчистка старых пакетов в кеше\033[0m \e[1;31mROOT\033[0m \e[1;32m✔\033[0m"
if [[ "$OS" == "arch_linux" ]]; then
echo -e "\e[1;35m5)\033[0m\e[1;37mСортировка mirrorlist по скорости либо по странам\033[0m \e[1;31mROOT\033[0m $ready_arch $fail_arch"
elif [[ "$OS" == "manjaro_linux" ]]; then
echo -e "\e[1;35m5)\033[0m\e[1;37mСортировка mirrorlist по скорости либо по странам\033[0m \e[1;31mROOT\033[0m $ready_manjaro $fail_manjaro" 
fi
echo -e "\e[1;35m6)\033[0m\e[1;37mИнициализация связки ключей (Возможно поможет при проблеме с ключами)\033[0m \e[1;31mROOT\033[0m \e[1;32m✔\033[0m"
echo -e "\e[1;35m7)\033[0m\e[1;37mУдаление .pacnew файлов в /etc\033[0m \e[1;31mROOT\033[0m \e[1;32m✔\033[0m"
echo -e "\e[1;35m8)\033[0m\e[1;37mРазблокировать DB пакмана\033[0m \e[1;31mROOT\033[0m \e[1;32m✔\033[0m"
echo -e "\e[1;35m9)\033[0m\e[1;37mПолучение PKGBUILD из AUR\033[0m $ready10 $fail10"
echo -e "\e[1;35m10)\033[0m\e[1;37mУстановка пакетов из списка (только пакеты,которые находятся в репозиториях)\033[0m \e[1;31mROOT\033[0m \e[1;32m✔\033[0m"
echo -e "\e[1;35m11)\033[0m\e[1;37mПереустановка всех пакетов из репозиториев\033[0m \e[1;31mROOT\033[0m \e[1;32m✔\033[0m"
echo -e "\e[1;35m12)\033[0m\e[1;37mДобавление неофициальных ключей\033[0m \e[1;31mROOT\033[0m \e[1;32m✔\033[0m"
echo -e "\e[1;35m13)\033[0m\e[1;37mСоздание локального репозитория для пакмана\033[0m \e[1;31mROOT\033[0m  $ready11 $fail11"
echo -e "\e[1;35m14)\033[0m\e[1;37mПоиск и удаления сирот\033[0m \e[1;31mROOT\033[0m \e[1;32m✔\033[0m"
if   [[ "$OS" == "arch_linux" ]]; then
echo -e "\e[1;35m15)\033[0m\e[1;37mПолучение PKGBUILD repository\033[0m $fail10 $ready10"
fi
echo -e "\e[1;31mQ)\033[0m\e[1;37mВыход\033[0m"
read -p "Ваш выбор:" pacman_helper
if [[ $pacman_helper == 1 ]]; then
paclog-pkglist >> ~/paclog-pkglist.edit
echo "line with spaces" | cut -d' ' -f1 ~/paclog-pkglist.edit >> ~/paclog-pkglist.list
rm ~/paclog-pkglist.edit
elif [[ $pacman_helper == 2 ]]; then
	sh /usr/local/bin/pkglist.sh
elif [[ $pacman_helper == 3 ]]; then
	sudo lostfiles
elif [[ $pacman_helper == 4 ]]; then
	sudo pacman -Sc
elif [[ $pacman_helper == 5 ]]; then
if   [[ "$OS" == "arch_linux" ]]; then
	echo "Как сортируем зеркала по скорости или по странам?" 
	until [[ "$answer_user_arch" == C  || "$answer_user_arch" == c ||  "$answer_user_arch" == S || "$answer_user_arch" == s  ]]
	do
		read -p "C - по странам ; S - по скорости: " answer_user_arch
	done
	if [[ $answer_user_arch == C ]] || [[ $answer_user_arch == c ]]; then
	sudo fetchmirrors && sudo pacman -Syy
elif [[ $answer_user_arch == S ]] || [[ $answer_user_arch == s ]]; then
	sudo reflector --verbose --protocol http --protocol https --latest 5 --sort rate --save /etc/pacman.d/mirrorlist && sudo pacman -Syy
elif [[ "$OS" == "manjaro_linux" ]]; then
	echo "Как сортируем зеркала по скорости или по странам?"
	until [[ "$answer_user_manjaro" == C || "$answer_user_manjaro" == c ||  "$answer_user_manjaro" == S || "$answer_user_manjaro" == s ]]
	do
	read -p "C - по странам ; S - по скорости: " answer_user_manjaro
	done
	if [[ $answer_user_manjaro == C ]] || [[ $answer_user_manjaro == c ]]; then
	echo	"Впишите ее например:Germany,France,Austria"
	read -r "country_var_manjaro"
	sudo pacman-mirrors --country "$country_var_manjaro" && sudo pacman -Syy
	fi
elif [[ $answer_user_manjaro == S ]] || [[ $answer_user_manjaro == s ]]; then
sudo pacman-mirrors --fasttrack 3 && sudo pacman -Syy
fi
fi
elif [[ $pacman_helper == 6 ]]; then
	if   [[ "$OS" == "arch_linux" ]]; then
	sudo pacman -Sy archlinux-keyring gnupg
	sudo rm -r /etc/pacman.d/gnupg/
	sudo pacman-key --init
	sudo pacman-key --populate archlinux
	sudo pacman-key --refresh-keys
elif [[ "$OS" == "manjaro_linux" ]]; then
	sudo pacman -Sy archlinux-keyring manjaro-keyring gnupg
	sudo rm -r /etc/pacman.d/gnupg/
	sudo pacman-key --init
	sudo pacman-key --populate archlinux manjaro
	sudo pacman-key --refresh-keys
fi
elif [[ $pacman_helper == 7 ]]; then
	sudo find /etc/ -name *.pacnew -delete
elif [[ $pacman_helper == 8 ]]; then
if [ -n "$(pidof pacman)" ]; then 
	echo -e "\e[1;33mВнимание!!!\033[0mПакман запущен завершите с ним работу"
else
	sudo rm /var/lib/pacman/db.lck
fi
elif [[ $pacman_helper == 9 ]]; then
cd ~/
	echo "Введите имя пакета:"
read -r "package_name_aur_var"
git clone https://aur.archlinux.org/"$package_name_aur_var".git
elif [[ $pacman_helper == 10 ]]; then
echo "Введите полный путь до pkglist:"
echo "Можно просто вставить Ctrl+Shift+V"
echo "Что бы выйти напишите Q"
while [[ $path_to_pkglist == * ]]
do
	read -r "path_to_pkglist"
	if [[ $path_to_pkglist == Q ]]; then
		break
	else
		sudo pacman -S $(< "$path_to_pkglist") --needed
	fi
done
elif [[ $pacman_helper == 11  ]]; then
sudo pacman -Qqn | sudo pacman -S -
elif [[ $pacman_helper == 12 ]]; then
echo "Введите ключ:"
echo "Можно просто вставить Ctrl+Shift+V"
read -r "keyid"
sudo pacman-key --recv-keys "$keyid" && pacman-key --finger "$keyid" && sleep 10s && sudo pacman-key --lsign-key "$keyid"
elif [[ $pacman_helper == 13 ]]; then
echo "Выбирете, что надо сделать:"
echo "1-создание локального репозитория ; 2-Занесениие нового пакета в ДБ репозитория"
read -r "create_repo"
if [[ $create_repo == 1  ]]; then
#Образец
#sudo ufw allow from 192.168.0.4 to any port 8080 proto tcp ДЛЯ UFW
#sudo ufw allow in 8080/tcp
#OR
#-A TCP -s 192.168.0.0/16 -p tcp -m tcp --dport 8080 -j ACCEPT в /etc/iptables/iptables.rules ДЛЯ iptables
sudo mkdir -p /srv/http/LAN/pacman-cache-lan/x86_64/
sudo cp -n /var/cache/pacman/pkg/*.pkg.tar.xz /srv/http/LAN/pacman-cache-lan/x86_64/
sudo chown -R http:http /srv/http/*
sudo mv /etc/nginx/nginx.conf /etc/nginx/nginx.conf.pacnew
sudo wget https://gitlab.com/naruto522ru/pacman_helper/raw/master/nginx.conf -P /etc/nginx/
sudo chown root:root /etc/nginx/nginx.conf
sudo chmod 0644 /etc/nginx/nginx.conf
sudo sed -i s/LOCAL_IP/$IP/ /etc/nginx/nginx.conf
sudo repo-add -n /srv/http/LAN/pacman-cache-lan/x86_64/pacman-cache-lan.db.tar.gz /srv/http/LAN/pacman-cache-lan/x86_64/*.pkg.tar.xz
echo "На втором компе внутри LAN нужно ввести в pacman.conf следущее:"
echo "[pacman-cache-lan]
SigLevel = Never
Server = http://$IP:8080/\$repo/\$arch/"
echo "После этого произвести включение или старт демона на первом компьютере,а также добавить соответствующие правила в фаервол."
elif [[ $create_repo == 2 ]]; then
sudo cp -n /var/cache/pacman/pkg/*.pkg.tar.xz /srv/http/LAN/pacman-cache-lan/x86_64/
sudo chown -R http:http /srv/http/*
sudo repo-add -n /srv/http/LAN/pacman-cache-lan/x86_64/pacman-cache-lan.db.tar.gz /srv/http/LAN/pacman-cache-lan/x86_64/*.pkg.tar.xz
fi
elif [[ $pacman_helper == 14 ]]; then
if [[ ! -n $(pacman -Qdt) ]]; then
	echo "Пакеты сироты не найдены."
else
	pacman -Rns $(pacman -Qdtq)
fi
elif [[ $pacman_helper == 15 ]]; then
if  [[ "$OS" == "arch_linux" ]]; then
echo "Выбирете способ:"
read -p "1 - более быстрый (Рекомендуется) ; 2 - более медленный и больше места на ЖД требует: " method
if [[ $method == 1 ]]; then 
echo "Введите имя пакета:"
echo "Что бы выйти напишите Q"
while [[ $package_name_var == * ]]
do
	read -r "package_name_var"
	if [[ $package_name_var == Q || $package_name_var == q ]]; then
		break
	else
		cd ~/ && asp export "$package_name_var"
	fi
done
elif [[ $method == 2 ]]; then
cd ~/ && git clone https://git.archlinux.org/svntogit/packages.git 
fi
else
echo "У вас не Arch Linux!"
  fi
elif [[ $pacman_helper == Q || $pacman_helper == q ]]; then
 exit
fi
