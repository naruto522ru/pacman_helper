#!/bin/bash

echo "Step 1"
rm -r ~/download_package_cache/ && sleep 3s
mkdir ~/download_package_cache/

#Обновляем репозитории
#sudo pacman -Sy --noconfirm

#Core
pacman -Sl core | awk '{$1=""; print $0}' | grep -i установлен > ~/download_package_cache/installed_package_core.txt.1
#Extra
pacman -Sl extra | awk '{$1=""; print $0}' | grep -i установлен > ~/download_package_cache/installed_package_extra.txt.1
#Community
pacman -Sl community | awk '{$1=""; print $0}' | grep -i установлен > ~/download_package_cache/installed_package_community.txt.1
#Multilib
pacman -Sl multilib | awk '{$1=""; print $0}' | grep -i установлен > ~/download_package_cache/installed_package_multilib.txt.1

#Первую секцию и вторую можно объединить, НО не объединять 3 и 2 секцию.

#Core
cat ~/download_package_cache/installed_package_core.txt.1 | sed 's/установлен//g' | tr -d [] | awk '{$3=""; print $0}' | awk '$1=$1' > ~/download_package_cache/installed_package_core.txt.2
#Extra
cat ~/download_package_cache/installed_package_extra.txt.1 | sed 's/установлен//g' | tr -d [] | awk '{$3=""; print $0}' | awk '$1=$1' > ~/download_package_cache/installed_package_extra.txt.2
#Community
cat ~/download_package_cache/installed_package_community.txt.1 | sed 's/установлен//g' | tr -d [] | awk '{$3=""; print $0}' | awk '$1=$1' > ~/download_package_cache/installed_package_community.txt.2
#Multilib
cat ~/download_package_cache/installed_package_multilib.txt.1 | sed 's/установлен//g' | tr -d [] | awk '{$3=""; print $0}' | awk '$1=$1' > ~/download_package_cache/installed_package_multilib.txt.2
#--------------------------------------------------------------------------------------------------------------|
#Для устаревших пакетов, которых нет в репозиториях, но есть в https://archive.archlinux.org/packages/         |
#Core                                                                                                          |
cat ~/download_package_cache/installed_package_core.txt.2 | awk '$2=$3' | awk '{$3=""; print $0}' > ~/download_package_cache/package_core_archive.txt
#Extra
cat ~/download_package_cache/installed_package_extra.txt.2 | awk '$2=$3' | awk '{$3=""; print $0}' > ~/download_package_cache/package_extra_archive.txt
#Community
cat ~/download_package_cache/installed_package_community.txt.2 | awk '$2=$3' | awk '{$3=""; print $0}' > ~/download_package_cache/package_community_archive.txt
#Multilib
cat ~/download_package_cache/installed_package_multilib.txt.2 | awk '$2=$3' | awk '{$3=""; print $0}' > ~/download_package_cache/package_multilib_archive.txt
#--------------------------------------------------------------------------------------------------------------
#Для актульных пакетов в системе
cat ~/download_package_cache/installed_package_*.txt.2 | awk '{$3=""; print $0}' | awk '$1=$1' | awk '{$2=""; print $0}'| awk '$1=$1' | sort > ~/download_package_cache/installed_package_all.temp
cat ~/download_package_cache/package_*_archive.txt | awk '{$2=""; print $0}' | awk '$1=$1' | sort > ~/download_package_cache/package_all_archive.txt
comm -23 ~/download_package_cache/installed_package_all.temp ~/download_package_cache/package_all_archive.txt > ~/download_package_cache/actual_repo_package.txt
sudo pacman -Sw --asdeps - < ~/download_package_cache/actual_repo_package.txt
#Удаление лишних файлов
sleep 10s && rm ~/download_package_cache/{installed_package_*.txt.*,package_all_archive.txt}

#Заметка для себя минус состоит в том, что загружает все пакеты, которые установленные через офф репозитория не взирая на версию. В качестве решения можно использовать sudo pacman -Sc --noconfirm, но это крайная мера.
#Пример допустим актульная версия пакета в репозитории 2, а у тебя установлена 1, а я хочу сделать так чтобы если версия пакета актуальная, то загружала через репозиторий, а если нет, то через мою приблуду, которая пока еще не написана полностью.

#cat ~/download_package_cache/package_core_archive.txt | awk -F"=" '{ print  "pkgname=" $1}'

echo ""
echo "Done Step 1"
echo ""
echo "Step 2"

echo ""
echo "Done Step 2"

